﻿using UnityEngine;
using System.Collections;

public class LightControler : MonoBehaviour {

    public Light lightPlayer;
    public Light lightAI;
    private Vector3 offset;         
    private Vector3 offsetAI;
    public Transform player;
    public Transform AI;

    // Use this for initialization
    void Start () {
        offset = lightPlayer.transform.position - player.position;
        offsetAI = lightAI.transform.position - AI.position;
    }
	
	// Update is called once per frame
	void Update () {
        lightPlayer.transform.position = player.position + offset;
        lightAI.transform.position = AI.position + offsetAI;
    }
}
