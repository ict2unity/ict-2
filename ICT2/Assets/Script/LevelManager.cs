﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class LevelManager : MonoBehaviour {
    //initialization
    public Transform mainMenu;
    public GameObject mainMenuPanel;
    public GameObject helpMenu;
    public GameObject selectLevelMenu;
    public GameObject creditMenu;

    //at the start of the game
    void Start()
    {
        mainMenuPanel.SetActive(true);
        helpMenu.SetActive(false);
        creditMenu.SetActive(false);
        selectLevelMenu.SetActive(false);
    }

    //Play Game
	public void LoadScene(string name){
        Application.LoadLevel(name);
        //SceneManager.LoadScene(name);
	}

    //When going to Main menu
    public void MainMenu()
    {
        mainMenuPanel.SetActive(true);
        helpMenu.SetActive(false);
        creditMenu.SetActive(false);
        selectLevelMenu.SetActive(false);
    }

    //When going to Help Menu
    public void HelpMenu()
    {
        mainMenuPanel.SetActive(false);
        helpMenu.SetActive(true);
        creditMenu.SetActive(false);
        selectLevelMenu.SetActive(false);
    }

    //When going to Select Level Menu
    public void SelectLevelMenu()
    {
        mainMenuPanel.SetActive(false);
        helpMenu.SetActive(false);
        creditMenu.SetActive(false);
        selectLevelMenu.SetActive(true);
    }

    //When going to Credit Menu
    public void CreditMenu()
    {
        mainMenuPanel.SetActive(false);
        helpMenu.SetActive(false);
        creditMenu.SetActive(true);
        selectLevelMenu.SetActive(false);
    }

    //Quit the Game
	public void QuitGame(){
		Application.Quit();
	}


}
