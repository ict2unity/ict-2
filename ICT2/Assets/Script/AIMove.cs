﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AIMove : MonoBehaviour {

    //import script and animator
    public BuffDebuffUI UIscript;
    public Die diceScript;
    public die dice;
    public Animator animator;
    public GameSequence gameSequence;
    public PlayerMove playerScript;
    public soundEffect soundEffectScript;

    //material
    public Material VerticalTrapMaterial;
    public Material BeastJailMaterial;
    public Material CheckpointMaterial;
    public Material VerticalShortcutMaterial;
    public Material buffMaterial;
    public Material debuffMaterial;
    public Material drawMaterial;

    //variable for camera controller
    public CameraController camControl;
    
    //variable for UI message
    public UIMessage message;

    //variable to move the player
    public Transform[] tilePoint;
    public int currentPoint;
    public die dieRoll;
    public Vector3 startPos;
    public Vector3 targetPos;
    public Vector3 curLoc;
    public Vector3 prevLoc;

    //variable to track buff/debuff on the player
    public bool immunityStatus;
    public int immunityCountDown;
    public bool spaceSnailStatus;
    public int spaceSnailCountDown;
    public bool beastJailStatus;
    public int beastJailCountDown;
    public bool dragBackStatus;

    //variables for sequence controller
    public int timeController = 0;
    public float dieTimer = 3.0f;
    public bool dieRolled;
    public int effectController = 0;

    //variables for card tracker
    public float drawCardRNG;
    public int verticalShortcut;
    public int immunity;
    public int horizontalSwap;
    public int verticalTrap;
    public int dragBack;
    public int spaceSnail;
    public int beastJail;

    //variables for tile recognition;
    public GameObject[] drawCardTiles;
    public GameObject[] buffDeployTiles;
    public GameObject[] debuffDeployTiles;
    public GameObject finishTile;
    public string enemyCurrentTile;

    //variables for AI controller;
    public float chooseCard;

    [SerializeField]
    public GameObject queryBodyParts;
    CharacterController controller;


    // Use this for initialization
    void Start ()
    {
        timeController = 0;
        effectController = 0;
        queryBodyParts = GameObject.FindWithTag("Enemy");
        startPos = tilePoint[0].position;
        startPos.y = 1;
        transform.position = startPos;
        currentPoint = 0;
        immunityStatus = false;
        dieRolled = false;
        dragBackStatus = false;
        if(SceneManager.GetActiveScene().name == "spacepirate3")
        {
            float temp;
            temp = Random.Range(1.00f, 4.00f);
            temp = (int)temp;
            if (temp == 1) { verticalShortcut = 1; spaceSnail = 1; }
            else if (temp == 2) { verticalShortcut = 1; beastJail = 1; }
            else if (temp == 3) { immunity = 1; spaceSnail = 1; }
            else if (temp == 4) { immunity = 1; beastJail = 1; }

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (dieRolled == false && gameSequence.playerTurn == false && dieTimer <=0.0f)
        {
            UIscript.rollButton.SetActive(false);
            dice.RollDieAI();
            dieRolled = true;
        }
        dieTimer -= Time.deltaTime;
        if (dieTimer <= 0.0f && timeController == 1 && gameSequence.playerTurn == false)
        {
            message.emptyingText();
            move();
            timeController = 2;
        }
        if (timeController == 2 && gameSequence.playerTurn == false)
        {
            camControl.camTransIncam3();
            if(camControl.cam1.transform.position == camControl.cam3.transform.position)
            {
                timeController = 3;
            }
        }

        if (timeController == 3 && gameSequence.playerTurn == false)
        {
            moveForward();
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(transform.position - prevLoc), 2 * Time.deltaTime);
            if (transform.position == targetPos)
            {
                message.displayPlayerTurn(); //displaying "Player Turn"
                camControl.checkCam3Pos();
                camControl.changeToMainCam();
                checkPosition();
                timeController = 4;
            }
        }
        if (timeController >= 4 && timeController <= 5 && gameSequence.playerTurn == false)
        {
            camControl.camTransOut();
            if(camControl.cam1.transform.position == camControl.cam1Init.transform.position)
            {
                enemyAction();
            }
            
        }
        if (timeController == 0 && gameSequence.playerTurn == true)
        {
            if (effectController == 0)
            {
                animator.SetInteger("AnimIndex", 4);
            }
        }
    }

    public void enemyAction()
    {
        if (enemyCurrentTile == "draw")
        {
            drawCard();
            Debug.Log("VS: " + verticalShortcut);
            Debug.Log("Immu: " + immunity);
            Debug.Log("HS: " + horizontalSwap);
            Debug.Log("VT: " + verticalTrap);
            Debug.Log("DB: " + dragBack);
            Debug.Log("SS: " + spaceSnail);
            Debug.Log("BJ: " + beastJail);
            if (immunityStatus)
            {
                immunityCountDown -= 1;
                if (immunityCountDown <= 0) { immunityStatus = false; }
            }
            if (spaceSnailStatus)
            {
                spaceSnailCountDown -= 1;
                if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
            }
            if (beastJailStatus)
            {
                beastJailCountDown -= 1;
                if (beastJailCountDown <= 0) { beastJailStatus = false; }
            }
            UIscript.rollButton.SetActive(true);
            timeController = 0;
            dieRolled = false;
            gameSequence.playerTurn = true;
        }

        else if (enemyCurrentTile == "buff")
        {
            if (verticalShortcut == 0 && immunity == 0 && horizontalSwap == 0)
            {
                if (immunityStatus)
                {
                    immunityCountDown -= 1;
                    if (immunityCountDown <= 0) { immunityStatus = false; }
                }
                if (spaceSnailStatus)
                {
                    spaceSnailCountDown -= 1;
                    if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
                }
                if (beastJailStatus)
                {
                    beastJailCountDown -= 1;
                    if (beastJailCountDown <= 0) { beastJailStatus = false; }
                }
                UIscript.rollButton.SetActive(true);
                timeController = 0;
                dieRolled = false;
                gameSequence.playerTurn = true;
            }
            else
            {
                chooseCard = Random.Range(1.00f, 4.00f);
                chooseCard = (int)chooseCard;
                if (chooseCard == 1 && verticalShortcut > 0)
                {
                    verticalShortcutClick();
                    Debug.Log("VS used");
                }
                else if (chooseCard == 2 && immunity > 0)
                {
                    immunityFuncion();
                    Debug.Log("Immu used");
                }
                else if (chooseCard == 3 && horizontalSwap > 0)
                {
                    horizontalSwapFuncion();
                    Debug.Log("HS used");
                }
            }
        }

        else if (enemyCurrentTile == "debuff")
        {
            if (verticalTrap == 0 && dragBack == 0 && spaceSnail == 0 && beastJail == 0)
            {
                if (immunityStatus)
                {
                    immunityCountDown -= 1;
                    if (immunityCountDown <= 0) { immunityStatus = false; }
                }
                if (spaceSnailStatus)
                {
                    spaceSnailCountDown -= 1;
                    if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
                }
                if (beastJailStatus)
                {
                    beastJailCountDown -= 1;
                    if (beastJailCountDown <= 0) { beastJailStatus = false; }
                }
                UIscript.rollButton.SetActive(true);
                timeController = 0;
                dieRolled = false;
                gameSequence.playerTurn = true;
            }
            else
            {
                chooseCard = Random.Range(1.00f, 4.00f);
                chooseCard = (int)chooseCard;
                if (chooseCard == 1 && verticalTrap > 0)
                {
                    verticalTrapFunction();
                    Debug.Log("VT used");
                }
                else if (chooseCard == 2 && dragBack > 0)
                {
                    dragBackFunction();

                    Debug.Log("DB used");
                }
                else if (chooseCard == 3 && spaceSnail > 0)
                {
                    spaceSnailFunction();
                    Debug.Log("SS used");
                }
                else if (chooseCard == 4 && beastJail > 0)
                {
                    beastJailFunction();
                    Debug.Log("BJ used");
                }
            }
        }

    }

    public void verticalTrapFunction()
    {
        message.displayEVT();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToAICam();
        Renderer changetile;
        verticalTrap -= 1;
        var temp = diceScript.value;
        var deployOn = playerScript.currentPoint + temp;
        tilePoint[deployOn].tag = "vt";
        changetile = tilePoint[deployOn].GetComponent<Renderer>();
        changetile.material = VerticalTrapMaterial;
        Debug.Log(tilePoint[deployOn] + "is tagged as vertical trap!");
        UIscript.rollButton.SetActive(true);
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        dieRolled = false;
        gameSequence.playerTurn = true;
    }

    public void dragBackFunction()
    {
        message.displayEDB();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToAICam();
        dragBack -= 1;
        playerScript.dragBackStatus = true;
        UIscript.rollButton.SetActive(true);
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        dieRolled = false;
        gameSequence.playerTurn = true;
    }

    public void spaceSnailFunction()
    {
        message.displayESS();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToAICam();
        spaceSnail -= 1;
        playerScript.spaceSnailStatus = true;
        playerScript.spaceSnailCountDown = 3;
        UIscript.rollButton.SetActive(true);
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        dieRolled = false;
        gameSequence.playerTurn = true;
    }

    public void beastJailFunction()
    {
        message.displayEBJ();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToAICam();
        Renderer changetile;
        beastJail -= 1;
        var temp = diceScript.value;
        var deployOn = playerScript.currentPoint + temp;
        tilePoint[deployOn].tag = "bj";
        changetile = tilePoint[deployOn].GetComponent<Renderer>();
        changetile.material = BeastJailMaterial;
        Debug.Log(tilePoint[deployOn] + "is tagged as beast jail!");
        UIscript.rollButton.SetActive(true);
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        dieRolled = false;
        gameSequence.playerTurn = true;
    }

    public void verticalShortcutClick()
    {
        message.displayEVS();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToAICam();
        Renderer changetile;
        verticalShortcut -= 1;
        var temp = diceScript.value;
        var deployOn = currentPoint + temp;
        tilePoint[deployOn].tag = "vs";
        changetile = tilePoint[deployOn].GetComponent<Renderer>();
        changetile.material = VerticalShortcutMaterial;
        Debug.Log(tilePoint[deployOn] + "is tagged as vertical shortcut!");
        UIscript.rollButton.SetActive(true);
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        dieRolled = false;
        gameSequence.playerTurn = true;
    }

    public void immunityFuncion()
    {
        message.displayEImmu();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToAICam();
        immunity -= 1;
        immunityStatus = true;
        immunityCountDown = 3;
        UIscript.rollButton.SetActive(true);
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        dieRolled = false;
        gameSequence.playerTurn = true;
    }

    public void horizontalSwapFuncion()
    {
        message.displayEHS();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToAICam();
        horizontalSwap -= 1;
        if (playerScript.immunityStatus)
        {
            playerScript.immunityStatus = false;
            playerScript.immunityCountDown = 0;
        }
        else
        {
            Vector3 temp1 = new Vector3();
            Vector3 temp2 = new Vector3();
            int temp3 = 0;
            int temp4 = 0;
            temp1 += queryBodyParts.transform.position;
            temp2 += playerScript.queryBodyParts.transform.position;
            queryBodyParts.transform.position = temp2;
            playerScript.queryBodyParts.transform.position = temp1;
            temp3 += currentPoint;
            temp4 += playerScript.currentPoint;
            currentPoint = temp4;
            playerScript.currentPoint = temp3;
        }
        UIscript.rollButton.SetActive(true);
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        dieRolled = false;
        gameSequence.playerTurn = true;
    }

    public void moveForward()
    {
        camControl.changeToAICam(); //camera zoom up to character

        //assign character position to variable
        prevLoc = curLoc;
        curLoc = transform.position;

        //if dice more than final position
        if (currentPoint > 90)
        {
            targetPos = tilePoint[90].position; //target tile is the last tile
        }
        else
        {
            targetPos = tilePoint[currentPoint].position; //target tile based on diceValue
        }
        targetPos.y = 1; //move character position in y direction

        //move the character to the target tile
        curLoc = Vector3.MoveTowards(transform.position, targetPos, 3 * Time.deltaTime);
        transform.position = curLoc;
    }

    //function that check on what tile the player is on
    public void checkPosition()
    {
        if (queryBodyParts.transform.position.x == finishTile.transform.position.x && queryBodyParts.transform.position.z == finishTile.transform.position.z)
        {
            Debug.Log("Enemy is on Finish Tile!");
            timeController = 0;
            message.displayLose();
            soundEffectScript.playYouLose();
        }
        // Starting code of Vertical Trap implementation
        if (tilePoint[currentPoint].CompareTag("vt"))
        {
            Debug.Log("Enemy is on Vertical Trap!");
            tilePoint[currentPoint].tag = "Untagged";
            Renderer changetile;
            changetile = tilePoint[currentPoint].GetComponent<Renderer>();
            if (enemyCurrentTile == "buff")
            {
                changetile.material = buffMaterial;
            }
            else if (enemyCurrentTile == "debuff")
            {
                changetile.material = debuffMaterial;
            }
            else if (enemyCurrentTile == "draw")
            {
                changetile.material = drawMaterial;
            }
            int temp1 = 0;
            bool a = true;
            if (immunityStatus)
            {
                immunityStatus = false;
                immunityCountDown = 0;
            }
            else
            {
                for (int i = 0; i < gameSequence.row2.Length; i++)
                {
                    if (gameSequence.row2[i] == tilePoint[currentPoint])
                    {
                        temp1 = i;
                        Vector3 temp2 = new Vector3();
                        temp2 += gameSequence.row1[i].transform.position;
                        temp2.y = 1;
                        queryBodyParts.transform.position = temp2;
                        for (int j = 0; j < tilePoint.Length; j++)
                        {
                            if (gameSequence.row1[i] == tilePoint[j])
                            {
                                currentPoint = j;
                                a = false;
                            }
                        }
                    }
                }
                if (a)
                {
                    for (int i = 0; i < gameSequence.row3.Length; i++)
                    {
                        if (gameSequence.row3[i] == tilePoint[currentPoint])
                        {
                            temp1 = i;
                            Vector3 temp2 = new Vector3();
                            temp2 += gameSequence.row2[i].transform.position;
                            temp2.y = 1;
                            queryBodyParts.transform.position = temp2;
                            for (int j = 0; j < tilePoint.Length; j++)
                            {
                                if (gameSequence.row2[i] == tilePoint[j])
                                {
                                    currentPoint = j;
                                    a = false;
                                }
                            }
                        }
                    }
                }
                if (a)
                {
                    for (int i = 0; i < gameSequence.row4.Length; i++)
                    {
                        if (gameSequence.row4[i] == tilePoint[currentPoint])
                        {
                            temp1 = i;
                            Vector3 temp2 = new Vector3();
                            temp2 += gameSequence.row3[i].transform.position;
                            temp2.y = 1;
                            queryBodyParts.transform.position = temp2;
                            for (int j = 0; j < tilePoint.Length; j++)
                            {
                                if (gameSequence.row3[i] == tilePoint[j])
                                {
                                    currentPoint = j;
                                    a = false;
                                }
                            }
                        }
                    }
                }
                if (a)
                {
                    for (int i = 0; i < gameSequence.row5.Length; i++)
                    {
                        if (gameSequence.row5[i] == tilePoint[currentPoint])
                        {
                            temp1 = i;
                            Vector3 temp2 = new Vector3();
                            temp2 += gameSequence.row4[i].transform.position;
                            temp2.y = 1;
                            queryBodyParts.transform.position = temp2;
                            for (int j = 0; j < tilePoint.Length; j++)
                            {
                                if (gameSequence.row4[i] == tilePoint[j])
                                {
                                    currentPoint = j;
                                    a = false;
                                }
                            }
                        }
                    }
                }
                if (a)
                {
                    for (int i = 0; i < gameSequence.row6.Length; i++)
                    {
                        if (gameSequence.row6[i] == tilePoint[currentPoint])
                        {
                            temp1 = i;
                            Vector3 temp2 = new Vector3();
                            temp2 += gameSequence.row5[i].transform.position;
                            temp2.y = 1;
                            queryBodyParts.transform.position = temp2;
                            for (int j = 0; j < tilePoint.Length; j++)
                            {
                                if (gameSequence.row5[i] == tilePoint[j])
                                {
                                    currentPoint = j;
                                    a = false;
                                }
                            }
                        }
                    }
                }
            }
        } // Ending code of vertical trap implementation
        // Starting code of vertical shortcut implementation
        if (tilePoint[currentPoint].CompareTag("vs"))
        {
            Debug.Log("Enemy is on Vertical Shortcut!");
            tilePoint[currentPoint].tag = "Untagged";
            Renderer changetile;
            changetile = tilePoint[currentPoint].GetComponent<Renderer>();
            if (enemyCurrentTile == "buff")
            {
                changetile.material = buffMaterial;
            }
            else if (enemyCurrentTile == "debuff")
            {
                changetile.material = debuffMaterial;
            }
            else if (enemyCurrentTile == "draw")
            {
                changetile.material = drawMaterial;
            }
            int temp1 = 0;
            bool a = true;
            for (int i = 0; i < gameSequence.row1.Length; i++)
            {
                if (gameSequence.row1[i] == tilePoint[currentPoint])
                {
                    temp1 = i;
                    Vector3 temp2 = new Vector3();
                    temp2 += gameSequence.row2[i].transform.position;
                    temp2.y = 1;
                    queryBodyParts.transform.position = temp2;
                    for (int j = 0; j < tilePoint.Length; j++)
                    {
                        if (gameSequence.row2[i] == tilePoint[j])
                        {
                            currentPoint = j;
                            a = false;
                        }
                    }
                }
            }
            if (a)
            {
                for (int i = 0; i < gameSequence.row2.Length; i++)
                {
                    if (gameSequence.row2[i] == tilePoint[currentPoint])
                    {
                        temp1 = i;
                        Vector3 temp2 = new Vector3();
                        temp2 += gameSequence.row3[i].transform.position;
                        temp2.y = 1;
                        queryBodyParts.transform.position = temp2;
                        for (int j = 0; j < tilePoint.Length; j++)
                        {
                            if (gameSequence.row3[i] == tilePoint[j])
                            {
                                currentPoint = j;
                                a = false;
                            }
                        }
                    }
                }
            }
            if (a)
            {
                for (int i = 0; i < gameSequence.row3.Length; i++)
                {
                    if (gameSequence.row3[i] == tilePoint[currentPoint])
                    {
                        temp1 = i;
                        Vector3 temp2 = new Vector3();
                        temp2 += gameSequence.row4[i].transform.position;
                        temp2.y = 1;
                        queryBodyParts.transform.position = temp2;
                        for (int j = 0; j < tilePoint.Length; j++)
                        {
                            if (gameSequence.row4[i] == tilePoint[j])
                            {
                                currentPoint = j;
                                a = false;
                            }
                        }
                    }
                }
            }
            if (a)
            {
                for (int i = 0; i < gameSequence.row4.Length; i++)
                {
                    if (gameSequence.row4[i] == tilePoint[currentPoint])
                    {
                        temp1 = i;
                        Vector3 temp2 = new Vector3();
                        temp2 += gameSequence.row5[i].transform.position;
                        temp2.y = 1;
                        queryBodyParts.transform.position = temp2;
                        for (int j = 0; j < tilePoint.Length; j++)
                        {
                            if (gameSequence.row5[i] == tilePoint[j])
                            {
                                currentPoint = j;
                                a = false;
                            }
                        }
                    }
                }
            }
            if (a)
            {
                for (int i = 0; i < gameSequence.row5.Length; i++)
                {
                    if (gameSequence.row5[i] == tilePoint[currentPoint])
                    {
                        temp1 = i;
                        Vector3 temp2 = new Vector3();
                        temp2 += gameSequence.row6[i].transform.position;
                        temp2.y = 1;
                        queryBodyParts.transform.position = temp2;
                        for (int j = 0; j < tilePoint.Length; j++)
                        {
                            if (gameSequence.row6[i] == tilePoint[j])
                            {
                                currentPoint = j;
                                a = false;
                            }
                        }
                    }
                }
            }
        }// Ending code of vertical shortcut implementation
        //Starting code of beast jail implementation
        if (tilePoint[currentPoint].CompareTag("bj"))
        {
            Debug.Log("Enemy is on Beast Jail!");
            tilePoint[currentPoint].tag = "Untagged";
            beastJailStatus = true;
            beastJailCountDown = 3;
        }//Ending code of beast jail implementation
        //Starting code of overlap prevention
        if (queryBodyParts.transform.position == playerScript.queryBodyParts.transform.position)
        {
            Vector3 temp = new Vector3();
            if (SceneManager.GetActiveScene().name == "spacepirate")
            {
                playerScript.currentPoint -= 1;
                temp += tilePoint[playerScript.currentPoint].position;
                temp.y = 1;
                playerScript.queryBodyParts.transform.position = temp;
            }
            else
            {
                playerScript.currentPoint -= 2;
                temp += tilePoint[playerScript.currentPoint].position;
                temp.y = 1;
                playerScript.queryBodyParts.transform.position = temp;
            }
        }//Ending code of overlap prevention
        foreach (GameObject tile in drawCardTiles)
        {
            if (queryBodyParts.transform.position.x == tile.transform.position.x && queryBodyParts.transform.position.z == tile.transform.position.z)
            {
                enemyCurrentTile = "draw";
                Debug.Log("Enemy is on a " + enemyCurrentTile + " tile!");
            }
        }
        foreach (GameObject tile in buffDeployTiles)
        {
            if (queryBodyParts.transform.position.x == tile.transform.position.x && queryBodyParts.transform.position.z == tile.transform.position.z)
            {
                enemyCurrentTile = "buff";
                Debug.Log("Enemy is on a deploy " + enemyCurrentTile + " tile!");
            }
        }
        foreach (GameObject tile in debuffDeployTiles)
        {
            if (queryBodyParts.transform.position.x == tile.transform.position.x && queryBodyParts.transform.position.z == tile.transform.position.z)
            {
                enemyCurrentTile = "debuff";
                Debug.Log("Enemy is on a deploy " + enemyCurrentTile + " tile!");
            }
        }
    }

    //function to determine what card does the player get on Draw Card Tiles
    public void drawCard()
    {
        drawCardRNG = Random.Range(1.00f, 7.00f);
        drawCardRNG = (int)drawCardRNG;
        if (drawCardRNG == 1) { verticalShortcut++; }       //1
        else if (drawCardRNG == 2) { immunity++; }          //2
        else if (drawCardRNG == 3) { horizontalSwap++; }    //3
        else if (drawCardRNG == 4) { verticalTrap++; }      //4
        else if (drawCardRNG == 5) { dragBack++; }          //5
        else if (drawCardRNG == 6) { spaceSnail++; }        //6
        else if (drawCardRNG == 7) { beastJail++; }         //7
    }

    //function to get the dice value and move the player based on that value
    public void move()
    {
        var diceValue = diceScript.value;
        if (spaceSnailStatus)
        {
            if (immunityStatus)
            {
                spaceSnailStatus = false;
                spaceSnailCountDown = 0;
                immunityStatus = false;
                immunityCountDown = 0;
            }
            else
            {
                diceValue -= 3;
                if (diceValue <= 0) { diceValue = 1; }
                spaceSnailCountDown -= 1;
                if (spaceSnailCountDown <= 0) {spaceSnailStatus = false;}
            }
        }
        if (dragBackStatus)
        {
            if (immunityStatus)
            {
                dragBackStatus = false;
                immunityStatus = false;
                immunityCountDown = 0;
            }
            else
            {
                diceValue = diceValue * -1;
                dragBackStatus = false;
            }
        }
        if (beastJailStatus)
        {
            if (immunityStatus)
            {
                beastJailStatus = false;
                beastJailCountDown = 0;
                immunityStatus = false;
                immunityCountDown = 0;
            }
            else
            {
                diceValue = 0;
                beastJailCountDown -= 1;
                if (beastJailCountDown <= 0)
                {
                    beastJailStatus = false;
                    Renderer changetile;
                    changetile = tilePoint[currentPoint].GetComponent<Renderer>();
                    if (enemyCurrentTile == "buff")
                    {
                        changetile.material = buffMaterial;
                    }
                    else if (enemyCurrentTile == "debuff")
                    {
                        changetile.material = debuffMaterial;
                    }
                    else if (enemyCurrentTile == "draw")
                    {
                        changetile.material = drawMaterial;
                    }
                }
            }
        }
        currentPoint = currentPoint + diceValue;
        animator.SetInteger("AnimIndex", 2);
    }
}

