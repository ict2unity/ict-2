﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour
{
    //import script and animator
    public BuffDebuffUI UIscript;
    public Die diceScript;
    public Animator animator;
    public GameSequence gameSequence;
    public AIMove AIScript;
    public soundEffect soundEffectScript;

    //material
    public Material VerticalTrapMaterial;
    public Material BeastJailMaterial;
    public Material CheckpointMaterial;
    public Material VerticalShortcutMaterial;
    public Material buffMaterial;
    public Material debuffMaterial;
    public Material drawMaterial;

    //variable for camera controller
    public CameraController camControl;

    //variable for UI message
    public UIMessage message;

    //variable to move the player
    public Transform[] tilePoint;
    public int currentPoint;
    public die dieRoll;
    public Vector3 startPos;
    public Vector3 targetPos;
    public Vector3 curLoc;
    public Vector3 prevLoc;

    //variable to track buff/debuff on the player
    public bool immunityStatus;
    public int immunityCountDown;
    public bool spaceSnailStatus;
    public int spaceSnailCountDown;
    public bool beastJailStatus;
    public int beastJailCountDown;
    public bool dragBackStatus;

    //variables for sequence controller
    public int timeController = 0;
    public float dieTimer = 3.0f;
    public int effectController = 0;

    //variables for card tracker
    public float drawCardRNG;
    public int verticalShortcut;
    public int immunity;
    public int horizontalSwap;
    public int verticalTrap;
    public int dragBack;
    public int spaceSnail;
    public int beastJail;

    //variables for tile recognition;
    public GameObject[] drawCardTiles;
    public GameObject[] buffDeployTiles;
    public GameObject[] debuffDeployTiles;
    public GameObject finishTile;
    public string playerCurrentTile;

    [SerializeField]
    public GameObject queryBodyParts;
    CharacterController controller;


    // Use this for initialization
    void Start()
    {
        timeController = 0;
        effectController = 0;
        queryBodyParts = GameObject.FindWithTag("Player");
        startPos = tilePoint[0].position;
        startPos.y = 1;
        transform.position = startPos;
        currentPoint = 0;
        immunityStatus = false;
        dragBackStatus = false;
        UIscript.rollButton.SetActive(true);

        //for debug and demo purpose, will be deleted later
        //horizontalSwap = 1;
        //immunity = 1;
        //dragBack = 1;
        //spaceSnail = 1;
        //verticalTrap = 1;
        //beastJail = 1;
        //checkPoint = 1;
        //verticalShortcut = 1;
        
    }

    // Update is called once per frame
    void Update()
    {
        dieTimer -= Time.deltaTime;
        if (dieTimer <= 0.0f && timeController == 1 && gameSequence.playerTurn)
        {
            message.emptyingText();
            move();
            timeController = 2;
        }

        if (timeController == 2 && gameSequence.playerTurn)
        {
            camControl.camTransIncam2();
            if (camControl.cam1.transform.position == camControl.cam2.transform.position)
            {
                timeController = 3;
            }
        }

        if (timeController == 3 && gameSequence.playerTurn)
        {
            moveForward();
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(transform.position - prevLoc), 2 * Time.deltaTime);
            if (transform.position == targetPos)
            {
                message.displayAITurn();
                camControl.checkCam2Pos();
                camControl.changeToMainCam();
                checkPosition();
                timeController = 4;
            }
        }
        if (timeController >= 4 && timeController <= 5 && gameSequence.playerTurn)
        {
            camControl.camTransOut();
            if (camControl.cam1.transform.position == camControl.cam1Init.transform.position)
            {
                playerAction();
                effectController = 0;
            }
            
        }
        if(timeController == 0 && gameSequence.playerTurn == false)
        {
            if (effectController == 0)
            {
                animator.SetInteger("AnimIndex", 4);
            }
        }       
    }

    public void playerAction()
    {
        if (playerCurrentTile == "draw")
        {
            drawCard();
            //animator.SetInteger("AnimIndex", 305);
            //camControl.changeToCam2();
            if (immunityStatus)
            {
                immunityCountDown -= 1;
                if (immunityCountDown <= 0) { immunityStatus = false; }
            }
            if (spaceSnailStatus)
            {
                spaceSnailCountDown -= 1;
                if(spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
            }
            if (beastJailStatus)
            {
                beastJailCountDown -= 1;
                if (beastJailCountDown <= 0) { beastJailStatus = false; }
            }
            timeController = 0;
            gameSequence.playerTurn = false;
        }
        
        else if (playerCurrentTile == "buff")
        {
            if (verticalShortcut == 0 && immunity == 0 && horizontalSwap == 0)
            {
                if (immunityStatus)
                {
                    immunityCountDown -= 1;
                    if (immunityCountDown <= 0) { immunityStatus = false; }
                }
                if (spaceSnailStatus)
                {
                    spaceSnailCountDown -= 1;
                    if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
                }
                if (beastJailStatus)
                {
                    beastJailCountDown -= 1;
                    if (beastJailCountDown <= 0) { beastJailStatus = false; }
                }
                timeController = 0;
                gameSequence.playerTurn = false;
            }
        }

        else if (playerCurrentTile == "debuff")
        {
            if (verticalTrap == 0 && dragBack == 0 && spaceSnail == 0 && beastJail == 0)
            {
                if (immunityStatus)
                {
                    immunityCountDown -= 1;
                    if (immunityCountDown <= 0) { immunityStatus = false; }
                }
                if (spaceSnailStatus)
                {
                    spaceSnailCountDown -= 1;
                    if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
                }
                if (beastJailStatus)
                {
                    beastJailCountDown -= 1;
                    if (beastJailCountDown <= 0) { beastJailStatus = false; }
                }
                timeController = 0;
                gameSequence.playerTurn = false;
            }
        }

    }

    public void verticalTrapFunction()
    {
        message.displayVT();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToCam2();
        verticalTrap -= 1;
        Renderer changetile;
        var temp = diceScript.value;
        var deployOn = AIScript.currentPoint + temp;
        tilePoint[deployOn].tag = "vt";
        changetile = tilePoint[deployOn].GetComponent<Renderer>();
        changetile.material = VerticalTrapMaterial;
        Debug.Log(tilePoint[deployOn] + "is tagged as vertical trap!");
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        gameSequence.playerTurn = false;
    }

    public void dragBackFunction()
    {
        message.displayDB();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToCam2();
        dragBack -= 1;
        AIScript.dragBackStatus = true;
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        gameSequence.playerTurn = false;
    }

    public void spaceSnailFunction()
    {
        message.displaySS();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToCam2();
        spaceSnail -= 1;
        AIScript.spaceSnailStatus = true;
        AIScript.spaceSnailCountDown = 3;
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        gameSequence.playerTurn = false;
    }

    public void beastJailFunction()
    {
        message.displayBJ();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToCam2();
        Renderer changetile;
        beastJail -= 1;
        var temp = diceScript.value;
        var deployOn = AIScript.currentPoint + temp;
        tilePoint[deployOn].tag = "bj";
        changetile = tilePoint[deployOn].GetComponent<Renderer>();
        changetile.material = BeastJailMaterial;
        Debug.Log(tilePoint[deployOn] + "is tagged as beast jail!");
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        gameSequence.playerTurn = false;
    }

    public void verticalShortcutClick()
    {
        message.displayVS();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToCam2();
        Renderer changetile;
        verticalShortcut -= 1;
        var temp = diceScript.value;
        var deployOn = currentPoint + temp;
        tilePoint[deployOn].tag = "vs";
        changetile = tilePoint[deployOn].GetComponent<Renderer>();
        changetile.material = VerticalShortcutMaterial;
        Debug.Log(tilePoint[deployOn] + "is tagged as vertical shortcut!");
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        gameSequence.playerTurn = false;
    }

    public void immunityFuncion()
    {
        message.displayImmu();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToCam2();
        immunity -= 1;
        immunityStatus = true;
        immunityCountDown = 3;
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        gameSequence.playerTurn = false;
    }

    public void horizontalSwapFuncion()
    {
        message.displayHS();
        effectController = 1;
        animator.SetInteger("AnimIndex", 305);
        //camControl.changeToCam2();
        horizontalSwap -= 1;
        if (AIScript.immunityStatus)
        {
            AIScript.immunityStatus = false;
            AIScript.immunityCountDown = 0;
        }
        else
        {
            Vector3 temp1 = new Vector3();
            Vector3 temp2 = new Vector3();
            int temp3 = 0;
            int temp4 = 0;
            temp1 += queryBodyParts.transform.position;
            temp2 += AIScript.queryBodyParts.transform.position;
            queryBodyParts.transform.position = temp2;
            AIScript.queryBodyParts.transform.position = temp1;
            temp3 += currentPoint;
            temp4 += AIScript.currentPoint;
            currentPoint = temp4;
            AIScript.currentPoint = temp3;
        }
        if (immunityStatus)
        {
            immunityCountDown -= 1;
            if (immunityCountDown <= 0) { immunityStatus = false; }
        }
        if (spaceSnailStatus)
        {
            spaceSnailCountDown -= 1;
            if (spaceSnailCountDown <= 0) { spaceSnailStatus = false; }
        }
        if (beastJailStatus)
        {
            beastJailCountDown -= 1;
            if (beastJailCountDown <= 0) { beastJailStatus = false; }
        }
        timeController = 0;
        gameSequence.playerTurn = false;
    }

    public void moveForward()
    {
        camControl.changeToCam2(); //camera zoom up to character

        //assign character position to variable
        prevLoc = curLoc; 
        curLoc = transform.position;

        //if dice more than final position
        if (currentPoint > 90)
        {
            targetPos = tilePoint[90].position; //target tile is the last tile
        }
        else
        {
            targetPos = tilePoint[currentPoint].position; //target tile based on diceValue
        }
        targetPos.y = 1; //move character position in y direction

        //move the character to the target tile
        curLoc = Vector3.MoveTowards(transform.position, targetPos, 3 * Time.deltaTime);
        transform.position = curLoc;
    }

    //function that check on what tile the player is on
    public void checkPosition()
    {
        
        if (queryBodyParts.transform.position.x == finishTile.transform.position.x && queryBodyParts.transform.position.z == finishTile.transform.position.z)
        {
            Debug.Log("Player is on Finish Tile!");
            timeController = 0;
            message.displayWin();
            soundEffectScript.playYouWin();
            message.NextLevelEnable();
        }
        // Starting code of vertical trap implementation
        if (tilePoint[currentPoint].CompareTag("vt"))
        {
            Debug.Log("Player is on Vertical Trap!");
            tilePoint[currentPoint].tag = "Untagged";
            Renderer changetile;
            changetile = tilePoint[currentPoint].GetComponent<Renderer>();
            if (playerCurrentTile == "buff")
            {
                changetile.material = buffMaterial;
            }
            else if (playerCurrentTile == "debuff")
            {
                changetile.material = debuffMaterial;
            }
            else if (playerCurrentTile == "draw")
            {
                changetile.material = drawMaterial;
            }
            int temp1 = 0;
            bool a = true;
            if (immunityStatus)
            {
                immunityStatus = false;
                immunityCountDown = 0;
            }
            else
            {
                   for (int i = 0; i < gameSequence.row2.Length; i++)
                    {
                        if (gameSequence.row2[i] == tilePoint[currentPoint])
                        {
                            temp1 = i;
                            Vector3 temp2 = new Vector3();
                            temp2 += gameSequence.row1[i].transform.position;
                            temp2.y = 1;
                            queryBodyParts.transform.position = temp2;
                            for (int j = 0; j < tilePoint.Length; j++)
                            {
                                if (gameSequence.row1[i] == tilePoint[j])
                                {
                                    currentPoint = j;
                                    a = false;
                                }
                            }
                        }
                    }
                if (a)
                {
                    for (int i = 0; i < gameSequence.row3.Length; i++)
                    {
                        if (gameSequence.row3[i] == tilePoint[currentPoint])
                        {
                            temp1 = i;
                            Vector3 temp2 = new Vector3();
                            temp2 += gameSequence.row2[i].transform.position;
                            temp2.y = 1;
                            queryBodyParts.transform.position = temp2;
                            for (int j = 0; j < tilePoint.Length; j++)
                            {
                                if (gameSequence.row2[i] == tilePoint[j])
                                {
                                    currentPoint = j;
                                    a = false;
                                }
                            }
                        }
                    }
                }
                if (a)
                {
                    for (int i = 0; i < gameSequence.row4.Length; i++)
                    {
                        if (gameSequence.row4[i] == tilePoint[currentPoint])
                        {
                            temp1 = i;
                            Vector3 temp2 = new Vector3();
                            temp2 += gameSequence.row3[i].transform.position;
                            temp2.y = 1;
                            queryBodyParts.transform.position = temp2;
                            for (int j = 0; j < tilePoint.Length; j++)
                            {
                                if (gameSequence.row3[i] == tilePoint[j])
                                {
                                    currentPoint = j;
                                    a = false;
                                }
                            }
                        }
                    }
                }
                if (a)
                {
                    for (int i = 0; i < gameSequence.row5.Length; i++)
                    {
                        if (gameSequence.row5[i] == tilePoint[currentPoint])
                        {
                            temp1 = i;
                            Vector3 temp2 = new Vector3();
                            temp2 += gameSequence.row4[i].transform.position;
                            temp2.y = 1;
                            queryBodyParts.transform.position = temp2;
                            for (int j = 0; j < tilePoint.Length; j++)
                            {
                                if (gameSequence.row4[i] == tilePoint[j])
                                {
                                    currentPoint = j;
                                    a = false;                                    
                                }
                            }
                        }
                    }
                }
                if (a)
                {
                    for (int i = 0; i < gameSequence.row6.Length; i++)
                    {
                        if (gameSequence.row6[i] == tilePoint[currentPoint])
                        {
                            temp1 = i;
                            Vector3 temp2 = new Vector3();
                            temp2 += gameSequence.row5[i].transform.position;
                            temp2.y = 1;
                            queryBodyParts.transform.position = temp2;
                            for (int j = 0; j < tilePoint.Length; j++)
                            {
                                if (gameSequence.row5[i] == tilePoint[j])
                                {
                                    currentPoint = j;
                                    a = false;
                                }
                            }
                        }
                    }
                }
            }
        }// Ending code of vertical trap implementation
        // Starting code of vertical shortcut implementation
        if (tilePoint[currentPoint].CompareTag("vs"))
        {
            Debug.Log("Player is on Vertical Shortcut!");
            tilePoint[currentPoint].tag = "Untagged";
            Renderer changetile;
            changetile = tilePoint[currentPoint].GetComponent<Renderer>();
            if (playerCurrentTile == "buff")
            {
                changetile.material = buffMaterial;
            }
            else if (playerCurrentTile == "debuff")
            {
                changetile.material = debuffMaterial;
            }
            else if (playerCurrentTile == "draw")
            {
                changetile.material = drawMaterial;
            }
            int temp1 = 0;
            bool a = true;
            for (int i = 0; i < gameSequence.row1.Length; i++)
            {
                if (gameSequence.row1[i] == tilePoint[currentPoint])
                {
                    temp1 = i;
                    Vector3 temp2 = new Vector3();
                    temp2 += gameSequence.row2[i].transform.position;
                    temp2.y = 1;
                    queryBodyParts.transform.position = temp2;
                    for (int j = 0; j < tilePoint.Length; j++)
                    {
                        if (gameSequence.row2[i] == tilePoint[j])
                        {
                            currentPoint = j;
                            a = false;
                        }
                    }
                }
            }
            if (a)
            {
                for (int i = 0; i < gameSequence.row2.Length; i++)
                {
                    if (gameSequence.row2[i] == tilePoint[currentPoint])
                    {
                        temp1 = i;
                        Vector3 temp2 = new Vector3();
                        temp2 += gameSequence.row3[i].transform.position;
                        temp2.y = 1;
                        queryBodyParts.transform.position = temp2;
                        for (int j = 0; j < tilePoint.Length; j++)
                        {
                            if (gameSequence.row3[i] == tilePoint[j])
                            {
                                currentPoint = j;
                                a = false;
                            }
                        }
                    }
                }
            }
            if (a)
            {
                for (int i = 0; i < gameSequence.row3.Length; i++)
                {
                    if (gameSequence.row3[i] == tilePoint[currentPoint])
                    {
                        temp1 = i;
                        Vector3 temp2 = new Vector3();
                        temp2 += gameSequence.row4[i].transform.position;
                        temp2.y = 1;
                        queryBodyParts.transform.position = temp2;
                        for (int j = 0; j < tilePoint.Length; j++)
                        {
                            if (gameSequence.row4[i] == tilePoint[j])
                            {
                                currentPoint = j;
                                a = false;
                            }
                        }
                    }
                }
            }
            if (a)
            {
                for (int i = 0; i < gameSequence.row4.Length; i++)
                {
                    if (gameSequence.row4[i] == tilePoint[currentPoint])
                    {
                        temp1 = i;
                        Vector3 temp2 = new Vector3();
                        temp2 += gameSequence.row5[i].transform.position;
                        temp2.y = 1;
                        queryBodyParts.transform.position = temp2;
                        for (int j = 0; j < tilePoint.Length; j++)
                        {
                            if (gameSequence.row5[i] == tilePoint[j])
                            {
                                currentPoint = j;
                                a = false;
                            }
                        }
                    }
                }
            }
            if (a)
            {
                for (int i = 0; i < gameSequence.row5.Length; i++)
                {
                    if (gameSequence.row5[i] == tilePoint[currentPoint])
                    {
                        temp1 = i;
                        Vector3 temp2 = new Vector3();
                        temp2 += gameSequence.row6[i].transform.position;
                        temp2.y = 1;
                        queryBodyParts.transform.position = temp2;
                        for (int j = 0; j < tilePoint.Length; j++)
                        {
                            if (gameSequence.row6[i] == tilePoint[j])
                            {
                                currentPoint = j;
                                a = false;
                            }
                        }
                    }
                }
            }
        }// Ending code of vertical shortcut implementation
        //Starting code of beast jail implementation
        if (tilePoint[currentPoint].CompareTag("bj"))
        {
            Debug.Log("Player is on Beast Jail!");
            tilePoint[currentPoint].tag = "Untagged";
            Renderer changetile;
            changetile = tilePoint[currentPoint].GetComponent<Renderer>();
            if (playerCurrentTile == "buff")
            {
                changetile.material = buffMaterial;
            }
            else if (playerCurrentTile == "debuff")
            {
                changetile.material = debuffMaterial;
            }
            else if (playerCurrentTile == "draw")
            {
                changetile.material = drawMaterial;
            }
            beastJailStatus = true;
            beastJailCountDown = 3;
        }//Ending code of beast jail implementation
        //Starting code of overlap prevention
        if(queryBodyParts.transform.position == AIScript.queryBodyParts.transform.position)
        {
            Vector3 temp = new Vector3();
            AIScript.currentPoint -= 1;
            temp += tilePoint[AIScript.currentPoint].position;
            temp.y = 1;
            AIScript.queryBodyParts.transform.position = temp;
        }//Ending code of overlap prevention

        foreach (GameObject tile in drawCardTiles)
        {
            if (queryBodyParts.transform.position.x == tile.transform.position.x && queryBodyParts.transform.position.z == tile.transform.position.z)
            {
                playerCurrentTile = "draw";
                Debug.Log("Player is on a " + playerCurrentTile + " tile!");
            }
        }
        foreach (GameObject tile in buffDeployTiles)
        {
            if (queryBodyParts.transform.position.x == tile.transform.position.x && queryBodyParts.transform.position.z == tile.transform.position.z)
            {
                playerCurrentTile = "buff";
                Debug.Log("Player is on a deploy " + playerCurrentTile + " tile!");
            }
        }
        foreach (GameObject tile in debuffDeployTiles)
        {
            if (queryBodyParts.transform.position.x == tile.transform.position.x && queryBodyParts.transform.position.z == tile.transform.position.z)
            {
                playerCurrentTile = "debuff";
                Debug.Log("Player is on a deploy " + playerCurrentTile + " tile!");
            }
        }
    }

    //function to determine what card does the player get on Draw Card Tiles
    public void drawCard()
    {
        drawCardRNG = Random.Range(1.00f, 7.00f);
        drawCardRNG = (int)drawCardRNG;
        if (drawCardRNG == 1) { verticalShortcut++; }       //1
        else if (drawCardRNG == 2) { immunity++; }          //2
        else if (drawCardRNG == 3) { horizontalSwap++; }    //3
        else if (drawCardRNG == 4) { verticalTrap++; }      //4
        else if (drawCardRNG == 5) { dragBack++; }          //5
        else if (drawCardRNG == 6) { spaceSnail++; }        //6
        else if (drawCardRNG == 7) { beastJail++; }         //7
        
    }

    //function to get the dice value and move the player based on that value
    public void move()
    {
        UIscript.rollButton.SetActive(false);
        var diceValue = diceScript.value;
        if (spaceSnailStatus)
        {
            if (immunityStatus)
            {
                spaceSnailStatus = false;
                spaceSnailCountDown = 0;
                immunityStatus = false;
                immunityCountDown = 0;
            }
            else
            {
                diceValue -= 3;
                if (diceValue <= 0) { diceValue = 1; }
                spaceSnailCountDown -= 1;
                if (spaceSnailCountDown <= 0) { spaceSnailStatus = false;}
            }
        }
        if (dragBackStatus)
        {
            if (immunityStatus)
            {
                dragBackStatus = false;
                immunityStatus = false;
                immunityCountDown = 0;
            }
            else
            {
                diceValue = diceValue * -1;
                dragBackStatus = false;
            }
        }
        if (beastJailStatus)
        {
            if (immunityStatus)
            {
                beastJailStatus = false;
                beastJailCountDown = 0;
                immunityStatus = false;
                immunityCountDown = 0;
            }
            else
            {
                diceValue = 0;
                beastJailCountDown -= 1;
                if (beastJailCountDown <= 0) { beastJailStatus = false;
                    Renderer changetile;
                    changetile = tilePoint[currentPoint].GetComponent<Renderer>();
                    if (playerCurrentTile == "buff")
                    {
                        changetile.material = buffMaterial;
                    }
                    else if (playerCurrentTile == "debuff")
                    {
                        changetile.material = debuffMaterial;
                    }
                    else if (playerCurrentTile == "draw")
                    {
                        changetile.material = drawMaterial;
                    }
                }
            }
        }
        currentPoint = currentPoint + diceValue;
        animator.SetInteger("AnimIndex", 2);
    }
}



