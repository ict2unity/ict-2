﻿using UnityEngine;
using System.Collections;

public class soundEffect : MonoBehaviour {

    public AudioSource audioSource;
    public AudioClip rollDieAudioClip;
    public AudioClip buttonClickClip;
    public AudioClip youWinClip;
    public AudioClip youLoseClip;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void playRollDieClip()
    {
        audioSource.clip = rollDieAudioClip;
        audioSource.Play();
    }

    public void playButtonClickClip()
    {
        audioSource.clip = buttonClickClip;
        audioSource.Play();
    }

    public void playYouWin()
    {
        audioSource.clip = youWinClip;
        audioSource.Play();
    }

    public void playYouLose()
    {
        audioSource.clip = youLoseClip;
        audioSource.Play();
    }
}
