﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuffDebuffUI: MonoBehaviour{
    //text for card counter
    public Text countTextvs;
    public Text countTextImmu;
    public Text countTextCkp;
    public Text countTexths;
    public Text countTextvt;
    public Text countTextdb;
    public Text countTextss;
    public Text countTextbj;

    //button & playerMove
    public GameObject vs;
    public GameObject Immu;
    public GameObject Ckp;
    public GameObject hs;
    public GameObject vt;
    public GameObject db;
    public GameObject ss;
    public GameObject bj;
    public PlayerMove pm;
    public GameObject rollButton;

    //use button
    public Button vsUse;
    public Button ImmuUse;
    public Button CkpUse;
    public Button hsUse;
    public Button vtUse;
    public Button dbUse;
    public Button ssUse;
    public Button bjUse;

    //cancel button
    public Button vsCancel;
    public Button ImmuCancel;
    public Button CkpCancel;
    public Button hsCancel;
    public Button vtCancel;
    public Button dbCancel;
    public Button ssCancel;
    public Button bjCancel;

    // Use this for initialization
    void Start () {
        rollButton.gameObject.SetActive(true);
        vs.gameObject.SetActive(false);
        Immu.gameObject.SetActive(false);
        Ckp.gameObject.SetActive(false);
        hs.gameObject.SetActive(false);
        vt.gameObject.SetActive(false);
        db.gameObject.SetActive(false);
        ss.gameObject.SetActive(false);
        bj.gameObject.SetActive(false);
        vsUse.interactable = false;
        vsCancel.interactable = false;
        ImmuUse.interactable = false;
        ImmuCancel.interactable = false;
        CkpUse.interactable = false;
        CkpCancel.interactable = false;
        hsUse.interactable = false;
        hsCancel.interactable = false;
        vtUse.interactable = false;
        vtCancel.interactable = false;
        dbUse.interactable = false;
        dbCancel.interactable = false;
        ssUse.interactable = false;
        ssCancel.interactable = false;
        
    }
	
	// Update is called once per frame
	void Update () {
        if (pm.verticalShortcut >= 1)
        {
            vs.gameObject.SetActive(true);
            countTextvs.text = pm.verticalShortcut.ToString();
        }
        else { vs.gameObject.SetActive(false); vsMouseExit(); }

        if (pm.immunity >= 1)
        {
            Immu.gameObject.SetActive(true);
            countTextImmu.text = pm.immunity.ToString();
        }
        else { Immu.gameObject.SetActive(false); ImmuMouseExit(); }

        if (pm.horizontalSwap >= 1)
        {
            hs.gameObject.SetActive(true);
            countTexths.text = pm.horizontalSwap.ToString();
        }
        else { hs.gameObject.SetActive(false); hsMouseExit(); }

        if (pm.verticalTrap >= 1)
        {
            vt.gameObject.SetActive(true);
            countTextvt.text = pm.verticalTrap.ToString();
        }
        else { vt.gameObject.SetActive(false); vtMouseExit(); }

        if (pm.dragBack >= 1)
        {
            db.gameObject.SetActive(true);
            countTextdb.text = pm.dragBack.ToString();
        }
        else { db.gameObject.SetActive(false); dbMouseExit(); }

        if (pm.spaceSnail >= 1)
        {
            ss.gameObject.SetActive(true);
            countTextss.text = pm.spaceSnail.ToString();
        }
        else { ss.gameObject.SetActive(false); ssMouseExit(); }

        if (pm.beastJail >= 1)
        {
            bj.gameObject.SetActive(true);
            countTextbj.text = pm.beastJail.ToString();
        }
        else { bj.gameObject.SetActive(false); bjMouseExit(); }
    }

    //Enlarge and move to center
    public void vsMouseEnter()
    {
        vs.transform.localScale = new Vector2(5, 5);
        vs.transform.position = new Vector2(683, 384);
        vsUse.interactable = true;
        vsCancel.interactable = true;
    }
    public void ImmuMouseEnter()
    {
        Immu.transform.localScale = new Vector2(5, 5);
        Immu.transform.position = new Vector2(683, 384);
        ImmuUse.interactable = true;
        ImmuCancel.interactable = true;
    }
    public void CkpMouseEnter()
    {
        Ckp.transform.localScale = new Vector2(5, 5);
        Ckp.transform.position = new Vector2(683, 384);
        CkpUse.interactable = true;
        CkpCancel.interactable = true;
    }
    public void hsMouseEnter()
    {
        hs.transform.localScale = new Vector2(5, 5);
        hs.transform.position = new Vector2(683, 384);
        hsUse.interactable = true;
        hsCancel.interactable = true;
    }
    public void vtMouseEnter()
    {
        vt.transform.localScale = new Vector2(5, 5);
        vt.transform.position = new Vector2(683, 384);
        vtUse.interactable = true;
        vtCancel.interactable = true;
    }
    public void dbMouseEnter()
    {
        db.transform.localScale = new Vector2(5, 5);
        db.transform.position = new Vector2(683, 384);
        dbUse.interactable = true;
        dbCancel.interactable = true;
    }
    public void ssMouseEnter()
    {
        ss.transform.localScale = new Vector2(5, 5);
        ss.transform.position = new Vector2(683, 384);
        ssUse.interactable = true;
        ssCancel.interactable = true;
    }
    public void bjMouseEnter()
    {
        bj.transform.localScale = new Vector2(5, 5);
        bj.transform.position = new Vector2(683, 384);
        bjUse.interactable = true;
        bjCancel.interactable = true;
    }

    //back to normal position
    public void vsMouseExit()
    {
        vs.transform.localScale = new Vector2(1, 1);
        vs.transform.position = new Vector2(79, 688);
        vsUse.interactable = false;
        vsCancel.interactable = false;
    }

    public void ImmuMouseExit()
    {
        Immu.transform.localScale = new Vector2(1, 1);
        Immu.transform.position = new Vector2(208, 688);
        ImmuUse.interactable = false;
        ImmuCancel.interactable = false;
    }

    public void CkpMouseExit()
    {
        Ckp.transform.localScale = new Vector2(1, 1);
        Ckp.transform.position = new Vector2(335, 688);
        CkpUse.interactable = false;
        CkpCancel.interactable = false;
    }

    public void hsMouseExit()
    {
        hs.transform.localScale = new Vector2(1, 1);
        hs.transform.position = new Vector2(335, 688);
        hsUse.interactable = false;
        hsCancel.interactable = false;
    }

    public void vtMouseExit()
    {
        vt.transform.localScale = new Vector2(1, 1);
        vt.transform.position = new Vector2(464, 688);
        vtUse.interactable = false;
        vtCancel.interactable = false;
    }

    public void dbMouseExit()
    {
        db.transform.localScale = new Vector2(1, 1);
        db.transform.position = new Vector2(590, 688);
        dbUse.interactable = false;
        dbCancel.interactable = false;
    }

    public void ssMouseExit()
    {
        ss.transform.localScale = new Vector2(1, 1);
        ss.transform.position = new Vector2(719, 688);
        ssUse.interactable = false;
        ssCancel.interactable = false;
    }

    public void bjMouseExit()
    {
        bj.transform.localScale = new Vector2(1, 1);
        bj.transform.position = new Vector2(844, 688);
        bjUse.interactable = false;
        bjCancel.interactable = false;
    }

}
    
