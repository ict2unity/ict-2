﻿using UnityEngine;
using System.Collections;

public class GameSequence : MonoBehaviour {

    public Transform[] row1;
    public Transform[] row2;
    public Transform[] row3;
    public Transform[] row4;
    public Transform[] row5;
    public Transform[] row6;

    public BuffDebuffUI UIscript;
    public bool playerTurn;

    // Use this for initialization
    void Start () {
        playerTurn = true;
        //showPlayerTurnMessage();      >> display a UI saying "Player Turn"
        //UIscript.rollButton.SetActive(true);
        //playerTurn();                 >> call a script of player turn sequence (roll die, move, draw card/deploy card, end turn)
        //showEnemyTurnMessage();       >> display a UI saying "Enemy Turn"
        //enemyTurn();                  >> call a script of enemy turn sequence (roll die, move, draw card/deploy card, end turn)
        //checkPosition();              >> if the player or the enemy reach the finish tile, display a UI saying "You win/lose!"
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
