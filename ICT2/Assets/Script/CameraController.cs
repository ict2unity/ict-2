﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    //cam 1 = main, cam2 = player1, cam 3 = AI
    public Camera cam1;
    public Camera cam2;
    public Camera cam3;
    public GameObject cam1Init;
    public Transform player;       //Public variable to store a reference to the player game object
    public Transform AI;            //Public variable to store a reference to the AI game object
    private Vector3 offset;         //Private variable to store the offset distance between the player and camera
    private Vector3 offsetAI;       //Private variable to store the offset distance between the AI and camera
    public float speed;
    private float journeyLength;

    // Use this for initialization
    void Start()
    {
        offset = cam2.transform.position - player.position;
        offsetAI = cam3.transform.position - AI.position;
        cam1.enabled = true;
        cam2.enabled = false;
        cam3.enabled = false;
    }

    void Update()
    {
        cam2.transform.position = player.position + offset;
        cam3.transform.position = AI.position + offsetAI;
    }

    public void changeToCam2()
    {
        cam1.enabled = false;
        cam2.enabled = true;
        cam3.enabled = false;
    }

    public void changeToMainCam()
    {
        cam1.enabled = true;
        cam2.enabled = false;
        cam3.enabled = false;
    }
    
    public void changeToAICam()
    {
        cam1.enabled = false;
        cam2.enabled = false;
        cam3.enabled = true;
    }
    public void checkCam2Pos()
    {
        cam1.transform.position = cam2.transform.position;
    }

    public void checkCam3Pos()
    {
        cam1.transform.position = cam2.transform.position;
    }

    public void camTransIncam2()
    {
        journeyLength = Vector3.Distance(cam1.transform.position, cam2.transform.position);
        float fracJourney = speed / journeyLength;
        cam1.transform.position = Vector3.Lerp(cam1.transform.position, cam2.transform.position, fracJourney);
        cam1.transform.rotation = Quaternion.Slerp(cam1.transform.rotation, cam2.transform.rotation, speed);
    }

    public void camTransIncam3()
    {
        journeyLength = Vector3.Distance(cam1.transform.position, cam3.transform.position);
        float fracJourney = speed / journeyLength;
        cam1.transform.position = Vector3.Lerp(cam1.transform.position, cam3.transform.position, fracJourney);
        cam1.transform.rotation = Quaternion.Slerp(cam1.transform.rotation, cam3.transform.rotation, speed);
    }

    public void camTransOut()
    {
        journeyLength = Vector3.Distance(cam1.transform.position, cam1Init.transform.position);
        float fracJourney = speed / journeyLength;
        cam1.transform.position = Vector3.Lerp(cam1.transform.position, cam1Init.transform.position, fracJourney);
        cam1.transform.rotation = Quaternion.Slerp(cam1.transform.rotation, cam1Init.transform.rotation, speed);
    }
}
