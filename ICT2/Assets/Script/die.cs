﻿using UnityEngine;
using System.Collections;

public class die : MonoBehaviour {

    public float torqueAmount = 10000.0f;
    public ForceMode forceMode;
    public PlayerMove playerScript;
    public AIMove AIScript;
    GameObject theDie;
    public soundEffect soundEffectScript;

    // Use this for initialization
    void Start () {

        transform.Rotate(xAngle: Random.Range(0.0f,500f), yAngle: Random.Range(0.0f, 500f), zAngle: Random.Range(0.0f, 500f));

    }
	
	// Update is called once per frame
	void Update () {	
	}

    //Function to roll the die
    public void RollDie()
    {
        Vector3 temp = new Vector3(-37.0f, 8.0f, 7.5f);
        theDie = GameObject.FindWithTag("Die");
        theDie.transform.position = temp;
        transform.Rotate(xAngle: Random.Range(0.0f, 500f), yAngle: Random.Range(0.0f, 500f), zAngle: Random.Range(0.0f, 500f));
        GetComponent<Rigidbody>().AddTorque(Random.onUnitSphere * torqueAmount, forceMode);
        playerScript.dieTimer = 2.0f;
        playerScript.timeController = 1;
        soundEffectScript.playRollDieClip();
    }

    public void RollDieAI()
    {
        Vector3 temp = new Vector3(-37.0f, 8.0f, 7.5f);
        theDie = GameObject.FindWithTag("Die");
        theDie.transform.position = temp;
        transform.Rotate(xAngle: Random.Range(0.0f, 500f), yAngle: Random.Range(0.0f, 500f), zAngle: Random.Range(0.0f, 500f));
        GetComponent<Rigidbody>().AddTorque(Random.onUnitSphere * torqueAmount, forceMode);
        AIScript.dieTimer = 2.0f;
        AIScript.timeController = 1;
        soundEffectScript.playRollDieClip();
    }
}
