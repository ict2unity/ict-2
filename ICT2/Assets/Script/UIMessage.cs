﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIMessage : MonoBehaviour {
    public Text message;
    public Text winOrLose;
    public Text buffOrDebuff;
    public GameObject NextLevel;

    // Use this for initialization
    void Start () {
        message.text = "Player Turn";
        winOrLose.text = null;
        buffOrDebuff.text = null;
        NextLevel.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public void NextLevelEnable() //enable proceed to the next level
    {
        NextLevel.SetActive(true);
    }

    public void emptyingText()
    {
        buffOrDebuff.text = null;
    }
    //displaying player turn
    public void displayPlayerTurn()
    {
        message.text = "Player Turn";
    }

    public void displayAITurn()
    {
        message.text = "AI Turn";
    }

    //displaying win or lose
    public void displayWin()
    {
        winOrLose.text = "YOU WIN!";
    }
    public void displayLose()
    {
        winOrLose.text = "YOU LOSE!";
    }

    //displaying buff or debuff used by player
    public void displayVS()
    {
        buffOrDebuff.text = "Vertical Shortcut used!";
        buffOrDebuff.color = Color.green;
    }

    public void displayImmu()
    {
        buffOrDebuff.text = "Immunity used!";
        buffOrDebuff.color = Color.green;
    }

    public void displayCkp()
    {
        buffOrDebuff.text = "Checkpoint used!";
        buffOrDebuff.color = Color.green;
    }

    public void displayHS()
    {
        buffOrDebuff.text = "Swap used!";
        buffOrDebuff.color = Color.green;
    }

    public void displayVT()
    {
        buffOrDebuff.text = "Vertical Trap used!";
        buffOrDebuff.color = Color.red;
    }

    public void displayDB()
    {
        buffOrDebuff.text = "Drag Back used!";
        buffOrDebuff.color = Color.red;
    }

    public void displaySS()
    {
        buffOrDebuff.text = "Space Snail used!";
        buffOrDebuff.color = Color.red;
    }

    public void displayBJ()
    {
        buffOrDebuff.text = "Beast Jail used!";
        buffOrDebuff.color = Color.red;
    }

    //displaying buff debuff used by enemy
    public void displayEVS()
    {
        buffOrDebuff.text = "Enemy use Vertical Shortcut!";
        buffOrDebuff.color = Color.green;
    }

    public void displayEImmu()
    {
        buffOrDebuff.text = "Enemy use Immunity!";
        buffOrDebuff.color = Color.green;
    }

    public void displayECkp()
    {
        buffOrDebuff.text = "Enemy use Checkpoint!";
        buffOrDebuff.color = Color.green;
    }

    public void displayEHS()
    {
        buffOrDebuff.text = "Enemy use Swap!";
        buffOrDebuff.color = Color.green;
    }

    public void displayEVT()
    {
        buffOrDebuff.text = "Enemy use Vertical Trap!";
        buffOrDebuff.color = Color.red;
    }

    public void displayEDB()
    {
        buffOrDebuff.text = "Enemy use Drag Back!";
        buffOrDebuff.color = Color.red;
    }

    public void displayESS()
    {
        buffOrDebuff.text = "Enemy use Space Snail!";
        buffOrDebuff.color = Color.red;
    }

    public void displayEBJ()
    {
        buffOrDebuff.text = "Enemy use Beast Jail!";
        buffOrDebuff.color = Color.red;
    }
}
